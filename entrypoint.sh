#!/usr/bin/bash

function substitute {
    /usr/bin/envsubst < "${1}" > "${1}".tmp && mv "${1}".tmp "${1}";
}

substitute "${CONFIG_FILE}"

if [ $JMX_JAVA_AGENT_DEPLOYMENT == "true" ]; then 
    exec java -javaagent:/opt/avisi/jmx-prometheus-exporter/jmx-prometheus-exporter.jar=${METRICS_PORT}:${CONFIG_FILE} ${JAVA_OPTS} "$@"
fi
echo "Running jmx exporter remote server ..."

exec java -jar /opt/avisi/jmx-prometheus-exporter/jmx-prometheus-exporter-server.jar ${METRICS_PORT} ${CONFIG_FILE} ${JAVA_OPTS} "$@"

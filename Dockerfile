FROM registry.gitlab.com/avisi/base/java:8

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

EXPOSE 9404

ENV JMX_HOSTPORT=localhost:5555 \
    JMX_JAVA_AGENT_DEPLOYMENT='true'

ENV METRICS_PORT=9404 \
    JAVA_OPTS="" \
    CONFIG_DIR=/opt/avisi/jmx-prometheus-exporter/config \
    CONFIG_FILE=/opt/avisi/jmx-prometheus-exporter/config/default.yaml

ENV JMX_EXPORTER_VERSION=0.3.1 \
    JMX_EXPORTER_AGENT_SHA256=2a25e74e7af7f4e63c227bf5d0d0a4da9b6b146ce521eca58fcde3bf803f1974 \
    JMX_EXPORTER_HTTP_SHA256=8e2ff48d67c15f9c1902ab180dd1fb2d55a39c6d903338fe19823cee198ce9c0

RUN mkdir -p /opt/avisi/jmx-prometheus-exporter/config && chown avisi:avisi $CONFIG_DIR

# Install the JMX prometheus java agent
RUN curl -L -O https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent/${JMX_EXPORTER_VERSION}/jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar \
    && sha256sum jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar && echo "${JMX_EXPORTER_AGENT_SHA256} jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar" | sha256sum -c \
    && mv jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar /opt/avisi/jmx-prometheus-exporter/jmx-prometheus-exporter.jar

# Install the JMX prometheus java http server
RUN curl -L -O https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_httpserver/${JMX_EXPORTER_VERSION}/jmx_prometheus_httpserver-${JMX_EXPORTER_VERSION}-jar-with-dependencies.jar \
    && sha256sum jmx_prometheus_httpserver-${JMX_EXPORTER_VERSION}-jar-with-dependencies.jar && echo "${JMX_EXPORTER_HTTP_SHA256} jmx_prometheus_httpserver-${JMX_EXPORTER_VERSION}-jar-with-dependencies.jar" | sha256sum -c \
    && mv jmx_prometheus_httpserver-${JMX_EXPORTER_VERSION}-jar-with-dependencies.jar /opt/avisi/jmx-prometheus-exporter/jmx-prometheus-exporter-server.jar

USER root
RUN yum install -y gettext && yum clean all
USER avisi

COPY --chown=avisi:avisi configs/ $CONFIG_DIR
COPY --chown=avisi:avisi entrypoint.sh /usr/local/bin/jmx-exporter

ENTRYPOINT ["jmx-exporter"]

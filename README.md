#  Prometheus JMX exporter

This repository contains the Avisi PEC JDK Docker base image with the Prometheus JMX exporter installed.

It can be used as a standalone solution or (recommended) as a Java agent.

[![pipeline status](https://gitlab.com/avisi/pec/jmx-exporter/badges/master/pipeline.svg)](https://gitlab.com/avisi/pec/jmx-exporter/commits/master)

## Table of Contents

- [Usage](#usage)
  - [Dockerfile](#dockerfile)
  - [Remote JMX server](#Remote-JMX-server)
  - [Custom configuration](#Custom-configuration)
- [Metrics](#metrics)
- [Automated build](#automated-build)
- [Contribute](#contribute)
- [License](#license)

## Usage

If your executable jar is located at `test.jar`, execute the jar as `jmx-exporter -jar test.jar`.
Here, `jmx-exporter` is a wrapper around `java`. It will run the jar with the jmx-exporter configured as a javaagent.

Example for in Docker:

```bash
docker run -v $(pwd)/test.jar:/test.jar registry.gitlab.com/avisi/pec/jmx-exporter:latest -jar test.jar
```

### Dockerfile

```dockerfile
FROM registry.gitlab.com/avisi/pec/jmx-exporter:latest
COPY myapplication.jar application.jar

CMD ["-jar", "application.jar"]
```

The jmx-exporter also takes `JAVA_OPTS` and passes those to the jvm.

### Remote JMX server 

Example for monitoring a remote JMX server:

```yaml
version: '3'
services:
  jmx-exporter:
    image: registry.gitlab.com/avisi/pec/jmx-exporter:latest
    ports: ['9404:9404']
    environment:
      CONFIG_FILE: '/opt/avisi/jmx-prometheus-exporter/config/http-local.yaml'
      JMX_HOSTPORT: 'remote-server-hostname:5555'
      JMX_JAVA_AGENT_DEPLOYMENT: 'false'
```

### Custom configuration

You can supply custom configuration. By default the configuration file is located at `/opt/avisi/jmx-prometheus-exporter/config/config.yaml`.

See [jmx_exporter/example_configs](https://github.com/prometheus/jmx_exporter/tree/master/example_configs) for examples.

## Metrics

The metrics are availabe by default on port `9404`. This is configurable with the environment variable `METRICS_PORT`.

## Automated build

This image is build at least once a month automatically. All PR's are automatically build.

## Contribute

PRs accepted. All issues should be reported in the [Gitlab issue tracker](https://gitlab.com/avisi/pec/jmx-exporter/issues).

## License

[MIT © Avisi B.V.](LICENSE)
